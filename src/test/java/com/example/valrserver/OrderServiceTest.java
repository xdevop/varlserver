package com.example.valrserver;

import com.example.valrserver.data.Order;
import com.example.valrserver.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrderServiceTest {
    @Autowired
    OrderService orderService;

    @Test
    void testFlow() {
        //Test sellRequest
        Random rand = new Random();
        int randomNum = rand.nextInt((100000000 - 1000) + 1) + 1000;
        Order order = new Order("sell", "0.03286", "822242", "BTCZAR", null, null, "27018.83926", 1, randomNum, false);
        Order orderSaved = orderService.save(order);
        assertNotNull(orderSaved);
        String savedOrderID = orderSaved.getId();
        assertNotNull(savedOrderID);
        List<Order> orderHistory = orderService.findAll();
        //Test order history and success creation at the same time
        assertTrue(orderHistory.size() > 0 && orderHistory.stream().anyMatch(order1 -> order1.getId().equals(savedOrderID)));
        //Test buy request for the item on sale
        Order buyOrderSaved = orderService.buy(savedOrderID);
        assertNotNull(buyOrderSaved);
        String savedBuyOrderID = buyOrderSaved.getId();
        assertNotNull(savedBuyOrderID);
        List<Order> newOderHistory = orderService.findAll();
        //Test order history and success creation of new purchase order
        assertTrue(newOderHistory.size() > 1 && newOderHistory.stream().anyMatch(order1 -> order1.getId().equals(savedBuyOrderID)));
        //Test completeTransaction after purchase request is placed
        List<Order> completedOrders = orderService.completeTransaction(savedOrderID);
        assertEquals(2, completedOrders.size());
//        orderService.deleteAll();
    }

}
