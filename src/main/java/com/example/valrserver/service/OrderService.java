package com.example.valrserver.service;

import com.example.valrserver.data.Order;
import com.example.valrserver.exceptions.EntityNotFoundException;
import com.example.valrserver.repository.OrderRepo;
import com.google.gson.Gson;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private MongoTemplate mt;

    public List<Order> findAll() {
        return orderRepo.findAll();
    }

    public List<Order> findTraded() {
        /*Query query = new Query();
        query.addCriteria(Criteria.where("tradedAt").ne(null));
        return mt.find(query, Order.class);*/
        return orderRepo.findTraded();
    }

    public Order findById(String id) {
        return orderRepo.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public Order save(Order order) {
        return orderRepo.save(order);
    }
    public Order buy(String orderId) {
        Order order = orderRepo.findById(orderId).orElseThrow(EntityNotFoundException::new);
        JSONObject object = new JSONObject(new Gson().toJson(order));
        object.remove("id");
        Order buyOrder = new Gson().fromJson(object.toString(), Order.class);
        buyOrder.setSide("buy");
        return orderRepo.save(buyOrder);
    }

    public List<Order> completeTransaction(String orderId) {
        String time = new Date().toString();
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(orderId).and("side").is("sell").and("tradedAt").is(null));
        Order saleOrder = mt.findOne(query, Order.class);//Seller's order
        assert saleOrder != null;
        saleOrder.setTradedAt(time);
        saleOrder.setTakerSide("buy");
        Query buyOrderQuery = new Query();
        buyOrderQuery.addCriteria(Criteria.where("side").is("buy").and("sequenceId").is(saleOrder.getSequenceId()));
        Order buyOrder = mt.findOne(buyOrderQuery, Order.class);//Buyer's order
        assert buyOrder != null;
        buyOrder.setTradedAt(time);
        buyOrder.setTakerSide("buy");
        return orderRepo.saveAll(Arrays.asList(saleOrder, buyOrder));
    }

    public void deleteById(String id) {
        orderRepo.deleteById(id);
    }

    public void deleteAll() {
        Query query = new Query();
        query.addCriteria(Criteria.where("price").exists(true));
        mt.findAllAndRemove(query, Order.class);
    }
}
