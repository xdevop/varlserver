package com.example.valrserver.controllers;

import com.example.valrserver.data.Order;
import com.example.valrserver.service.OrderService;
import com.google.gson.Gson;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api/orders")
public class Orders {
    @Autowired
    private OrderService orderService;
    /*@GetMapping("/getOrderBook")
    public String getOrderBook() {
        return "Here are some order books";
    }*/

    @GetMapping("/getOrderBook")
    public List<Order> findAll() {
        return orderService.findAll();
    }

    @GetMapping("/tradeHistory")
    public List<Order> tradeHistory() {
        return orderService.findTraded();
    }

    @GetMapping("/{id}")
    public Order findById(@PathVariable String id) {
        return orderService.findById(id);
    }

    //Ask to sell
    @PostMapping("/buyRequest/{orderId}")
    public Order buyRequest( @PathVariable String orderId) {
        return orderService.buy(orderId);
    }

    //Ask to buy
    @PostMapping("/sellRequest")
    public Order sellRequest(@RequestBody Object object) {
        Order order = buildOrderFromRequest(new Gson().toJson(object));
        order.setSide("sell");
        return orderService.save(order);
    }

    //Complete transaction
    @PostMapping("/completeTransaction/{orderId}")
    public List<Order> completeTransaction(@PathVariable String orderId) {
        return orderService.completeTransaction(orderId);
    }

    @DeleteMapping("/delete/{id}")
    public Order deleteById(@PathVariable String id) {
        Order order = orderService.findById(id);
        order.setDeleted(true);
        return orderService.save(order);
//        orderService.deleteById(id);
    }

    @DeleteMapping("/deleteAll")//For tests to allow clearing DB 😃
    public List<Order> deleteAll() {
        orderService.deleteAll();
        return orderService.findAll();
    }

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<Exception> handleAllExceptions(RuntimeException ex) {
        return new ResponseEntity<Exception>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //Extracting payload attributes manually to avoid Versioning issues with Java POJOs in prod. This needs proper endpoint validation.
    private Order buildOrderFromRequest(String orderJson) {
        Random rand = new Random();
        int randomNum = rand.nextInt((100000000 - 1000) + 1) + 1000;
        JSONObject jsonObject = new JSONObject(orderJson);
        Order order = new Order("",
                jsonObject.getString("quantity"),
                jsonObject.getString("price"),
                jsonObject.getString("currencyPair"), null, "", "27018.83926",
                jsonObject.getInt("orderCount"), randomNum, false);
        return order;
    }
}
