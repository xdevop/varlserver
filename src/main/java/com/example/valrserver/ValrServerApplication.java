package com.example.valrserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValrServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ValrServerApplication.class, args);
    }

}
