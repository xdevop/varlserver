package com.example.valrserver.repository;

import com.example.valrserver.data.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface OrderRepo extends MongoRepository<Order, String> {
    @Query("{'tradedAt' : {$ne: null }}")
    List<Order> findTraded();
}
