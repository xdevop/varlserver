package com.example.valrserver.data;

import org.springframework.data.annotation.Id;

public class Order {
    @Id
    String id;
    String side, quantity, price, currencyPair, tradedAt = null, takerSide = null, quoteVolume = "140491.8343218";
    int orderCount, sequenceId = 4395272;
    boolean deleted = false;

    public Order() {
    }

    public Order(String side, String quantity, String price, String currencyPair, String tradedAt, String takerSide, String quoteVolume, int orderCount, int sequenceId, boolean deleted) {
        this.side = side;
        this.quantity = quantity;
        this.price = price;
        this.currencyPair = currencyPair;
        this.tradedAt = tradedAt;
        this.takerSide = takerSide;
        this.quoteVolume = quoteVolume;
        this.orderCount = orderCount;
        this.sequenceId = sequenceId;
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getTradedAt() {
        return tradedAt;
    }

    public void setTradedAt(String tradedAt) {
        this.tradedAt = tradedAt;
    }

    public String getTakerSide() {
        return takerSide;
    }

    public void setTakerSide(String takerSide) {
        this.takerSide = takerSide;
    }

    public String getQuoteVolume() {
        return quoteVolume;
    }

    public void setQuoteVolume(String quoteVolume) {
        this.quoteVolume = quoteVolume;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public int getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", side='" + side + '\'' +
                ", quantity='" + quantity + '\'' +
                ", price='" + price + '\'' +
                ", currencyPair='" + currencyPair + '\'' +
                ", tradedAt='" + tradedAt + '\'' +
                ", takerSide='" + takerSide + '\'' +
                ", quoteVolume='" + quoteVolume + '\'' +
                ", orderCount=" + orderCount +
                ", sequenceId=" + sequenceId +
                ", deleted=" + deleted +
                '}';
    }
}
