package com.example.valrserver.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);
    private static final long JWT_EXPIRATION = 5 * 60 * 1000L;
    public static final String TOKEN_PREFIX = "Bearer ";

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    public String generateToken(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        String username = ((UserDetails) principal).getUsername();
        Date expireDate = new Date(System.currentTimeMillis() + JWT_EXPIRATION);
        try {
            // Create an instance of the signed algorithm
            Algorithm algorithm = Algorithm.HMAC256(jwtSecret);
            return JWT.create().withExpiresAt(expireDate).withClaim("username", username)
                    .sign(algorithm);
        } catch (JWTCreationException jwtCreationException) {
            logger.warn("Token creation failed");
            return null;
        }
    }

    public String getUsernameFromJWT(String authToken) {
        try {
            DecodedJWT jwt = JWT.decode(authToken);
            return jwt.getClaim("username").asString();
        } catch (JWTDecodeException jwtDecodeException) {
            logger.warn("Getting username from token failed");
            return null;
        }
    }

    public boolean validateToken(String authToken) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtSecret);
            JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(authToken);
            return true;
        } catch (JWTVerificationException jwtVerificationException) {
            logger.warn("Token verification");
            return false;
        }
    }
}