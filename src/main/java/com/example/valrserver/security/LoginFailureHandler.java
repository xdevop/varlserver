package com.example.valrserver.security;

import com.example.valrserver.common.CodedMessage;
import com.example.valrserver.common.APIResponseData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        APIResponseData responseBean = setResponseData(exception);
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(response.getWriter(), responseBean);
    }

    private APIResponseData setResponseData(AuthenticationException exception) {
        if (exception instanceof LockedException) {
            return APIResponseData.build(CodedMessage.USER_IS_LOCKED);
        } else if (exception instanceof CredentialsExpiredException) {
            return APIResponseData.build(CodedMessage.CREDENTIAL_NOT_RIGHT);
        } else if (exception instanceof AccountExpiredException) {
            return APIResponseData.build(CodedMessage.USER_NOT_ALLOWED);
        } else if (exception instanceof DisabledException) {
            return APIResponseData.build(CodedMessage.DISABLE_USER);
        } else if (exception instanceof BadCredentialsException) {
            return APIResponseData.build(CodedMessage.AUTH_ERROR);
        }
        return APIResponseData.build(CodedMessage.USER_NOT_LOGGED_IN);
    }

}
