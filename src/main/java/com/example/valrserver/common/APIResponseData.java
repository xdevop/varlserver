package com.example.valrserver.common;

public class APIResponseData {

    private String code = null;

    private String message = null;

    private Object data;

    public APIResponseData() {
        this.code = CodedMessage.SUCCESS_MESSAGE.getCode();
        this.message = CodedMessage.SUCCESS_MESSAGE.getMessage();
        this.data = null;
    }

    public APIResponseData(String code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    public APIResponseData(CodedMessage message, Object data) {
        this.code = message.getCode();
        this.message = message.getMessage();
        this.data = data;
    }

    public APIResponseData(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public APIResponseData(Object data) {
        this.code = CodedMessage.SUCCESS_MESSAGE.getCode();
        this.message = CodedMessage.SUCCESS_MESSAGE.getMessage();
        this.data = data;
    }

    public static APIResponseData build(CodedMessage message, Object data) {
        return new APIResponseData(message, data);
    }

    public static APIResponseData build(String code, String message, Object data) {
        return new APIResponseData(code, message, data);
    }

    public static APIResponseData build(CodedMessage message) {
        return new APIResponseData(message, null);
    }

    public static APIResponseData build(String code, String message) {
        return new APIResponseData(code, message, null);
    }

    public static APIResponseData ok(Object data) {
        return new APIResponseData(data);
    }

    public static APIResponseData ok() {
        return new APIResponseData(null);
    }

    /**
     * Set the return data to a system error state
     */
    public void changeToSysError() {
        this.code = CodedMessage.SYSTEM_ERROR.getCode();
        this.message = CodedMessage.SYSTEM_ERROR.getMessage();
    }

    public void fail() {
        this.code = CodedMessage.FAILED.getCode();
        this.message = CodedMessage.FAILED.getMessage();
    }

    public String getCode() {
        return code;
    }

    public void setCodeMessage(CodedMessage CodedMessage) {
        this.code = CodedMessage.getCode();
        this.message = CodedMessage.getMessage();
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
