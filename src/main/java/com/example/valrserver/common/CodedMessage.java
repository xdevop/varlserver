package com.example.valrserver.common;

public enum CodedMessage {
    /**
     * Permissions 10000-10020
     */
    SYSTEM_ERROR("10000", "System error"),
    /**
     * Login and users 10120-10150
     */
    USER_NOT_LOGGED_IN("10120", "No sign-in or sign-in timeout"),
    USER_CRED_EMPTY("10121", "The user name or password is empty"),
    AUTH_ERROR("10122", "The user name or password is incorrect"),
    USER_NOT_EXISTING("10123", "User not found"),
    USER_IS_EXISTING("10124", "User found"),
    DISABLE_USER("10125", "The user is disabled or logged out"),
    USER_IS_ALLOWED("10126", "Legitimate users"),
    USER_NOT_ALLOWED("10127", "The user is not legal"),
    CREDENTIAL_NOT_RIGHT("10131", "The password has expired"),
    USER_LOGOUT("10128", "Signed out"),
    USER_IS_LOCKED("10129", "Too many logins，The user is locked out"),
    USER_PWD_EQUAL("10130", "The original password is the same as the new password"),
    SUCCESS_MESSAGE("20000", "Succeeded"),
    FAILED("20001", "Failed");
    private final String code;
    private final String message;
    CodedMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }
    public String getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
