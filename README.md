# Giude

### Setting up DB

From project root, run:

```
docker-compose -f docker-compose-server.yml up 
```

### Test Procedure

A **.http** file ```generated-requests.http``` is included in the root of the project with all the requests to be made.

### Steps implemented are:

* Place a request to sell
* Place a request to buy against existing sale request above.
* Complete transaction simulating actual sale/purchase.

From steps above, we are able to get;

* **Order book**
* **Trade history**

### Assumptions:

* Buyers have sufficient balance
* Traders info and transaction records properly handled.
* Proper endpoint validations is implemented(due to dynamic extraction of request payloads).

### To Note:

* Token expires in 5 minutes.
* ```Authorization``` header is required for all requests after authentication.
* In-memory user is used for the test.

Get ```Bearer``` token via:

``` 
POST http://localhost:8080/auth/login
Content-Type: application/json

{
  "username": "valruser",
  "password": "123456"
}
```

Place a request to sell(Note the response `id` for the next step):

``` 
POST http://localhost:8080/api/orders/sellRequest
Content-Type: application/json
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM4NzczODcsInVzZXJuYW1lIjoidmFscnVzZXIifQ.h8OO6-laBtgD97mhVCsiyAWL0bmIueJ8GFB1pM7tMyg

{
  "quantity": "0.06",
  "price": "823833",
  "currencyPair": "BTCZAR",
  "orderCount": 1
}
```

Place a request to buy against the sale request using the ````id```` for ```sellRequest``` response.

``` 
POST http://localhost:8080/api/orders/buyRequest/6162fce23d076262fc7f2a02
Content-Type: application/json
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM4MTMwODEsInVzZXJuYW1lIjoidmFscnVzZXIifQ.CZ1x7Xo30xKufHw_RsjeZXUkVVD8FFtDXkWqjP0V0hk
```

Complete transaction using the ````id```` for ```sellRequest``` response

``` 
POST http://localhost:8080/api/orders/completeTransaction/6160a82b57a48521836d567b
Content-Type: application/json
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM4MTMwODEsInVzZXJuYW1lIjoidmFscnVzZXIifQ.CZ1x7Xo30xKufHw_RsjeZXUkVVD8FFtDXkWqjP0V0hk
```

Get Order book

``` 
GET http://localhost:8080/api/orders/getOrderBook
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM4ODcwNjYsInVzZXJuYW1lIjoidmFscnVzZXIifQ.0wKhTdwOCFDB33R3UspvNUoJcSmk_Z1Ju44jcYxV43s
```

Get trade history

``` 
GET http://localhost:8080/api/orders/tradeHistory
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzM4ODcwNjYsInVzZXJuYW1lIjoidmFscnVzZXIifQ.0wKhTdwOCFDB33R3UspvNUoJcSmk_Z1Ju44jcYxV43s
```

### A basic unit test is included

### Environment: (Java 11, JUnit 5, Gradle is used)